// __tests__/mutation.test.js

const xss = require('xss');
const { hasMutation } = require('../utils/isMutant');

test('get an empty DNA sequence', () => {
    const dna = [];
    const result = hasMutation(dna);

    expect(result).toBe(false);
});

test('get a DNA sequences without mutation', () => {
    const dna = [
        "ATGCGA", 
        "CAGTGC", 
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(false);
});

test('DNA sequence without mutation (horizontal)', () => {
    const dna = [
        "ATGCGA", 
        "CAGTGC", 
        "TTATTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(false);
});

test('DNA sequence with mutation (horizontal)', () => {
    const dna = [
        "ATGCGA", 
        "CAGTGC", 
        "TTTTTT",
        "AGACGG",
        "GCGTCA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(true);
});

test('DNA sequence with mutation (vertical)', () => {
    const dna = [
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "GCGTCA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(true);
});

test('DNA sequence with mutation (diagonal right-down)', () => {
    const dna = [
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "GCGTCA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(true);
});

test('get a DNA sequence with mutation', () => {
    const dna = [
        "ATGCGA",
        "CAGTGC",
        "TTATGT",
        "AGAAGG",
        "CCCCTA",
        "TCACTG"
    ];

    const result = hasMutation(dna);
    expect(result).toBe(true);
});

// Tests de seguridad
test('Can\'t be able to XSS attacks', () => {
  const maliciousInput = '<script>alert("Vulnerable")</script>';
  const sanitizedInput = xss(maliciousInput);
  expect(sanitizedInput).not.toMatch(/<script>/);
});

