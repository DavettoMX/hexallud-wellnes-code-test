# hexalud_take_home_code_test

This API was created as a solution to the technical test for ***Hexalud Wellnes*** for the position of ***Full-Stack Developer***.

The objective was to develop a project that detects if a person has genetic differences based on their
DNA sequence.


## Description
For this project two endpoints were created, a GET `/stats` method and a POST `/mutation` method.

For the database system SQLite was selected for the following reasons:

***Ease of use***: SQLite3 is known for its simplicity and ease of use. It requires no complex setup or constant management, making it suitable for small and medium projects.

***Portability***: SQLite3 stores the entire database in a single file, making it highly portable. You can easily copy and move the database without compatibility issues in different development and production environments.

***Low resource consumption***: SQLite3 is lightweight and has low resource consumption compared to larger and more complex database systems. This can be beneficial for projects deployed in services like Render, where you may have resource constraints.

## Installation
To install this project, we will have to create a folder in our local directory, once the folder is created, we open a terminal and go to the path of our folder

```
cd <project_directory>
```

Once located in the folder where we will have our project, next we must clone the GitLab repository in our local environment

Clone with SSH
```
git@gitlab.com:hexalud/hexalud_take_home_code_test.git
```

Clone with HTTP:
```
https://gitlab.com/hexalud/hexalud_take_home_code_test.git
```

Once our repository has been cloned we will execute the command:

```
npm install
```

To install all the necessary dependencies from our **package.json** file. Once the dependencies are installed we will execute the command:

```
npm start
```

And it will start the execution of our environment.

## Usage
To use the API in our local environment, we will use port 8000.

To use the API from the server we use the following link:

### Use GET `/stats` method
```
https://hexalud-code-test.onrender.com/stats
```

The GET `/stats` method returns a JSON with the count of mutations, non-mutations, and the radius.

### Use POST `/mutation` method
```
https://hexalud-code-test.onrender.com/mutation
```

The POST `/mutation` method receives a JSON in the following format:

```
{
         "dna": [
             "ATGCGA",
             "CAGTGC",
             "TTTTTT",
             "AGACGG",
             "GCGTCA",
             "TCACTG"
         ]
}
````

## Authors and acknowledgment
This project was created by ***Joshue Rogelio Garcia Butron***

## Project status
As this project is created using the free version of render, the server may stop due to inactivity. If that happens, please contact me to perform the deployment again.
