function hasMutation(dna) {
  const n = dna.length;

  // Función para verificar si una secuencia contiene más de cuatro letras iguales
  function hasSequence(sequence) {
    const regex = /(A{4,}|T{4,}|C{4,}|G{4,})/; // Expresión regular para buscar secuencias de 4 letras iguales

    if (cache[sequence] === undefined) {
      cache[sequence] = regex.test(sequence);
    }

    return cache[sequence];
  }

  // Crear un objeto para almacenar los resultados de las llamadas anteriores a la función `hasSequence()`
  const cache = {};

  // Verificar filas y columnas
  for (let i = 0; i < n; i++) {
    let row = '';
    let column = '';
    for (let j = 0; j < n; j++) {
      row += dna[i][j];
      column += dna[j][i];
    }
    if (hasSequence(row) || hasSequence(column)) {
      return true;
    }
  }

  // Verificar diagonales principales
  for (let i = 0; i <= n - 4; i++) {
    let diagonal = '';
    for (let j = 0; j < n - i; j++) {
      diagonal += dna[j][i + j];
    }
    if (hasSequence(diagonal)) {
      return true;
    }
  }

  // Verificar diagonales secundarias
  for (let i = 1; i <= n - 4; i++) {
    let diagonal = '';
    for (let j = 0; j < n - i; j++) {
      diagonal += dna[i + j][j];
    }
    if (hasSequence(diagonal)) {
      return true;
    }
  }

  return false;
}

module.exports = { hasMutation };