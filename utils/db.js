function database() {
    const sqlite3 = require('sqlite3').verbose();
    const db = new sqlite3.Database('adn.db');
    
    db.serialize(() => {
        db.run("CREATE TABLE IF NOT EXISTS adn (sequence TEXT, hasMutation BOOLEAN)");
    });
    
    return db;
}


module.exports = { database };