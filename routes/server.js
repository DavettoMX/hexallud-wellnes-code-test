const express = require('express');
const sqlite3 = require('sqlite3').verbose();
const app = express();
const port = 8000;

const { hasMutation } = require('../utils/isMutant');

const { database } = require('../utils/db');

// Middleware to let JSON be received
app.use(express.json());

// Create database
const db = database();

// Index
app.get('/', (req, res) => {
  res.json({ title: 'WELCOME TO MY HEXALUD TEST CODE - Joshue Garcia ' });
});

// Route to verify if a DNA sequence has mutation
app.post('/mutation', (req, res) => {
  const dnaSequence = req.body.dna;
  const isMutant = hasMutation(dnaSequence); // Reemplaza 'hasMutationFunction' por la función real

  // Insertar el registro en la base de datos
  const stmt = db.prepare("INSERT INTO adn VALUES (?, ?)");
  
  stmt.run(JSON.stringify(dnaSequence), isMutant);
  stmt.finalize();

  res.status(isMutant ? 200 : 403).json({ isMutant });
});

// Route to get the statistics of the DNA sequences
app.get('/stats', (req, res) => {

  db.get("SELECT COUNT(*) AS count FROM adn WHERE hasMutation = 1", (err, row) => {
    const count_mutations = row.count;

    db.get("SELECT COUNT(*) AS count FROM adn WHERE hasMutation = 0", (err, row) => {
      const count_no_mutation = row.count;

      const ratio = count_no_mutation === 0 ? count_mutations : count_mutations / count_no_mutation;

      res.json({ count_mutations, count_no_mutation, ratio });
    });
  });

});

// Start server
app.listen(port, () => {
  console.log(`Servidor iniciado en http://localhost:${port}`);
});

module.exports = app;