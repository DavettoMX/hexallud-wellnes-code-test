var express = require('express');
var router = express.Router();

const { hasMutation } = require('../utils/isMutant');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

/* POST mutation */
router.post('/mutation', function(req, res, next) {
  const dna = req.body.dna;
  const isMutant = hasMutation(dna);

  res.status(isMutant ? 200 : 403).json({ isMutant });
});

module.exports = router;
